#include <stdio.h>
#include <unistd.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <stdlib.h> 
#include <netinet/in.h> 
#include <string.h>
#include <sys/stat.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <fcntl.h>
#include <errno.h>

#define SERVER_PORT "8050"
#define BUFFER_SIZE 64
#define MAX_CLIENTS 20
#define LOCAL_ADDRESS "127.0.0.1"

char const* heartbeatPort;
int heartbeatSocket;
int serverSocket;
struct sockaddr_in heartbeatAddress;

void handleMessage(char* message) {
    write(1, message, strlen(message));
}

void handleMessageAndExit(char* message) {
    write(1, message, strlen(message));
    exit(EXIT_FAILURE);
}

void handleSignal() {
    char* serverPort = SERVER_PORT;
    socklen_t sendToOutput = sendto( heartbeatSocket, serverPort, strlen(serverPort), 0, (struct sockaddr *) &heartbeatAddress, sizeof(heartbeatAddress));
    if(sendToOutput != strlen(serverPort)) {
        close(heartbeatSocket);
        handleMessageAndExit("Failed to send heartbeat message.\n");
    }
    alarm(1);
}

void setSocketOptions(int socketNumber, int type) {
	//type=0 -> udp    type=1 -> tcp
	int opt1 = 1;
	int setSocketOptionOutput1 = setsockopt(socketNumber, SOL_SOCKET, SO_REUSEPORT, &opt1, sizeof(opt1));
    if (setSocketOptionOutput1 < 0) 
    { 
        handleMessageAndExit("Failed to set socket options.\n");
    }
	int opt2 = 1;
	int setSocketOptionOutput2 = setsockopt(socketNumber, SOL_SOCKET, SO_REUSEADDR, &opt2, sizeof(opt2));
    if (setSocketOptionOutput2 < 0) 
    { 
        handleMessageAndExit("Failed to set socket options.\n");
    }
	if(type) {
		return;
	}
	int opt3 = 1;
	int setSocketOptionOutput3 = setsockopt(socketNumber, SOL_SOCKET, SO_BROADCAST, &opt3, sizeof(opt3));
    if (setSocketOptionOutput3 < 0) 
    { 
        handleMessageAndExit("Failed to set socket options.\n");
    }
}

void sendHeartbeatSignal() {

    heartbeatSocket = socket(PF_INET, SOCK_DGRAM, 0);
    if(heartbeatSocket < 0)
    {
        close(heartbeatSocket);
        handleMessageAndExit("Failed to run socket for heartbeat.\n");
    }

    setSocketOptions(heartbeatSocket, 0);

    heartbeatAddress.sin_family = AF_INET;
    heartbeatAddress.sin_port = htons( (uint16_t)atoi(heartbeatPort) );
    heartbeatAddress.sin_addr.s_addr = htonl(INADDR_BROADCAST); 

    int heartbeatBindOutput = bind(heartbeatSocket, (struct sockaddr *) &heartbeatAddress, sizeof(heartbeatAddress));
    if (heartbeatBindOutput < 0) 
    { 
        close(heartbeatSocket);
        handleMessageAndExit("Failed to bind heartbeat socket.\n");
    }

    handleMessage("Server is online...\n");
    signal(SIGALRM, handleSignal);
    alarm(1);

}

void clearBuffer(char* buffer) {
    for(int i = 0; i < BUFFER_SIZE; i++) {
        buffer[i] = '\0';
    }
}

int checkStringsEqual(char* a, char* b) {
	if(strlen(a) != strlen(b)) {
		return 0;
	}
	for(int i = 0; i < strlen(a); i++) {
		if(a[i] != b[i]) {
			return 0;
		}
	}
	return 1;
}

void handleUpload(int clientSocket) {
    char* okResponse = "1";
    char* fileName = (char*) malloc(BUFFER_SIZE * sizeof(char));
    char* buffer = (char*) malloc(BUFFER_SIZE * sizeof(char));

    int sendOutput = send(clientSocket, okResponse, strlen(okResponse), 0);
    if(sendOutput < 0) {
        close(serverSocket);
        close(heartbeatSocket);
        handleMessageAndExit("Failed to send!\n");
    }

    int recvOutput = recv(clientSocket, fileName, BUFFER_SIZE, 0);
    if(recvOutput < 0) {
        close(serverSocket);
        close(heartbeatSocket);
        handleMessageAndExit("Failed to recieve!\n");
    }
    
    unlink(fileName);
    int file = open(fileName, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);
    clearBuffer(fileName);

    sendOutput = send(clientSocket, okResponse, strlen(okResponse), 0);
    if(sendOutput < 0) {
        close(serverSocket);
        close(heartbeatSocket);
        handleMessageAndExit("Failed to send!\n");
    }

    while((recvOutput = recv(clientSocket, buffer, BUFFER_SIZE, 0)) > 0) {
        if(recvOutput < 0) {
            close(serverSocket);
            close(heartbeatSocket);
            handleMessageAndExit("Failed to recieve!\n");
        }
        if(checkStringsEqual(buffer, "-->EOF<--")) {
            break;
        }
        write(file, buffer, strlen(buffer));
        sendOutput = send(clientSocket, okResponse, strlen(okResponse), 0);
        if(sendOutput < 0) {
            close(serverSocket);
            close(heartbeatSocket);
            handleMessageAndExit("Failed to send!\n");
        }
        clearBuffer(buffer);
    }
    close(file);
    char* successMessage = "The file upload was successful!\n";
    sendOutput = send(clientSocket, successMessage, strlen(successMessage), 0);
    if(sendOutput < 0) {
        close(serverSocket);
        close(heartbeatSocket);
        handleMessageAndExit("Failed to send!\n");
    }
    handleMessage(successMessage);
    
}

void handleDownload(int clientSocket) {
    char* okResponse = "1";
    char* badResponse = "0";
    char* fileName = (char*) malloc(BUFFER_SIZE * sizeof(char));
    char* clientResponse = (char*) malloc(BUFFER_SIZE * sizeof(char));
    char* buffer = (char*) malloc(BUFFER_SIZE * sizeof(char));
    
    int sendOutput = send(clientSocket, okResponse, strlen(okResponse), 0);
    if(sendOutput < 0) {
        close(serverSocket);
        close(heartbeatSocket);
        handleMessageAndExit("Failed to send!\n");
    }

    int recvOutput = recv(clientSocket, fileName, BUFFER_SIZE, 0);
    if(recvOutput < 0) {
        close(serverSocket);
        close(heartbeatSocket);
        handleMessageAndExit("Failed to recieve!\n");
    }
    
    int file = open(fileName, O_RDONLY, S_IRUSR | S_IWUSR);
    clearBuffer(fileName);
    if(file < 0) {
        sendOutput = send(clientSocket, badResponse, strlen(badResponse), 0);
        if(sendOutput < 0) {
            close(serverSocket);
            close(heartbeatSocket);
            handleMessageAndExit("Failed to send!\n");
        }
        int recvOutput = recv(clientSocket, clientResponse, BUFFER_SIZE, 0);
        if(recvOutput < 0) {
            close(serverSocket);
            close(heartbeatSocket);
            handleMessageAndExit("Failed to recieve!\n");
        }
        clearBuffer(clientResponse);
        handleMessage("A client requested a file that doesn't exist!\n");
        return;
    }

    sendOutput = send(clientSocket, okResponse, strlen(okResponse), 0);
    if(sendOutput < 0) {
        close(serverSocket);
        close(heartbeatSocket);
        handleMessageAndExit("Failed to send!\n");
    }

    while(read(file, buffer, BUFFER_SIZE) > 0) {
		sendOutput = send(clientSocket, buffer, strlen(buffer), 0);
		if(sendOutput < 0) {
			handleMessageAndExit("Failed to send!\n");
		}
		recvOutput = recv(clientSocket, clientResponse, BUFFER_SIZE, 0);
		if(recvOutput < 0) {
			handleMessageAndExit("Failed to recieve!\n");
		}
		clearBuffer(clientResponse);
		clearBuffer(buffer);
	}

	char* EOFMessage = "-->EOF<--";
	sendOutput = send(clientSocket, EOFMessage, strlen(EOFMessage), 0);
	if(sendOutput < 0) {
        handleMessageAndExit("Failed to send!\n");
    }
	recvOutput = recv(clientSocket, clientResponse, BUFFER_SIZE, 0);
	if(recvOutput < 0) {
        handleMessageAndExit("Failed to recieve!\n");
    }
	handleMessage(clientResponse);
	clearBuffer(clientResponse);
	close(file);
}

void startServer() {
    serverSocket = socket(PF_INET, SOCK_STREAM, 0);
    if(serverSocket < 0)
    {
        close(serverSocket);
        close(heartbeatSocket);
        handleMessageAndExit("Failed to run socket for server.\n");
    }

    setSocketOptions(serverSocket, 1);

    struct sockaddr_in serverAddress;
    int serverAddressSize = sizeof(serverAddress);
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons( atoi(SERVER_PORT) );
    serverAddress.sin_addr.s_addr = inet_addr(LOCAL_ADDRESS); 

    int serverBindOutput = bind(serverSocket, (struct sockaddr *) &serverAddress, sizeof(serverAddress));
    if (serverBindOutput < 0) 
    { 
        close(serverSocket);
        close(heartbeatSocket);
        handleMessageAndExit("Failed to bind server socket.\n");
    }
    
    int serverListenOutput = listen(serverSocket, MAX_CLIENTS);
    if (serverListenOutput < 0)
    {
        close(serverSocket);
        close(heartbeatSocket);
        handleMessageAndExit("Failed to listen server.\n");
    }
    handleMessage("Listening for new clients...\n");

    fd_set mainReadSockets, tempReadSockets;
    FD_ZERO(&mainReadSockets);
    FD_ZERO(&tempReadSockets);
    FD_SET(serverSocket, &tempReadSockets);
    int maxSocketNumber = serverSocket;

    struct sockaddr_in clientAddress;
    int clientAddressSize = sizeof(clientAddress);

    while(1) {
        mainReadSockets = tempReadSockets;
        int selectOutput = select(maxSocketNumber + 1, &mainReadSockets, NULL, NULL, NULL);
        if(selectOutput < 0) {
            continue;
        }
        for(int i = 0; i < maxSocketNumber + 1; i++) {
            if (FD_ISSET(i, &mainReadSockets)) {
                if (i == serverSocket) {
                    int newClientSocket = accept(serverSocket, (struct sockaddr *)&clientAddress, (socklen_t*)&clientAddressSize);
                    if (newClientSocket < 0)
                    {
                        close(serverSocket);
                        close(heartbeatSocket);
                        handleMessageAndExit("Failed to accept new client in server.\n");
                    }
                    else {
                        handleMessage("Successfully accepted a new client.\n");
                    }
                    FD_SET(newClientSocket, &tempReadSockets);
                    if (newClientSocket > maxSocketNumber) {
                        maxSocketNumber = newClientSocket;
                    }
                } 
                else {
                    char command[BUFFER_SIZE];
                    clearBuffer(command);
                    int recvOutput = recv(i, command, BUFFER_SIZE, 0);
                    if(recvOutput < 0) 
                    {
                        close(serverSocket);
                        close(heartbeatSocket);
                        close(i);
                        handleMessageAndExit("Failed to recieve from client.\n");
                    }
                    else if (recvOutput == 0)
                    {
                        handleMessage("A client disconnected from server.\n");
                        close(i);
                        FD_CLR(i, &tempReadSockets);
                        clearBuffer(command);
                    }
                    else {
                        if(checkStringsEqual(command, "upload")) {
                            handleUpload(i);
                        }
                        else if(checkStringsEqual(command, "download")) {
                            handleDownload(i);
                        }
                        else {
                            handleMessage("Command not found.\n");
                        }
                        clearBuffer(command);
                    }
                }
            }
        }
    }
}

int main(int argc, char const *argv[]) 
{ 
    heartbeatPort = argv[1];

    if(argc != 2) {
        handleMessageAndExit("The number of arguments passed is not correct!!\n");
    }

    sendHeartbeatSignal();

    startServer();

    close(serverSocket);
    close(heartbeatSocket);
    exit(EXIT_SUCCESS);

} 
