#include <stdio.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <unistd.h> 
#include <string.h> 
#include <stdlib.h> 
#include <signal.h>
#include <time.h>
#include <netinet/in.h> 
#include <sys/time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#define MAX_COMMAND_SIZE 32
#define BUFFER_SIZE 64
#define MAX_CLIENTS 20
#define LOCAL_ADDRESS "127.0.0.1"

struct sockaddr_in heartbeatAddress;
struct sockaddr_in serverAddress;
struct sockaddr_in broadcastAddress;
socklen_t broadcastAddressSize;
char const* heartbeatPort;
char const* broadcastPort;
char const* clientPort;
int serverPort, serverStatus;
int clientSocketForServer, broadcastSocket, clientSocketToTCPConnect;
int requestRecievedYet;
char* requestedFileNameForMe;
char* broadcastRequestMessage;

void handleMessage(char* message) {
    write(1, message, strlen(message));
}

void handleMessageAndExit(char* message) {
    write(1, message, strlen(message));
    exit(EXIT_FAILURE);
}

void setSocketOptions(int socketNumber, int type) {
	//type=0 -> udp    type=1 -> tcp
	int opt1 = 1;
	int setSocketOptionOutput1 = setsockopt(socketNumber, SOL_SOCKET, SO_REUSEPORT, &opt1, sizeof(opt1));
    if (setSocketOptionOutput1 < 0) 
    { 
        handleMessageAndExit("Failed to set socket options.\n");
    }
	int opt2 = 1;
	int setSocketOptionOutput2 = setsockopt(socketNumber, SOL_SOCKET, SO_REUSEADDR, &opt2, sizeof(opt2));
    if (setSocketOptionOutput2 < 0) 
    { 
        handleMessageAndExit("Failed to set socket options.\n");
    }
	if(type) {
		return;
	}
	int opt3 = 1;
	int setSocketOptionOutput3 = setsockopt(socketNumber, SOL_SOCKET, SO_BROADCAST, &opt3, sizeof(opt3));
    if (setSocketOptionOutput3 < 0) 
    { 
        handleMessageAndExit("Failed to set socket options.\n");
    }
}

int checkForServer() {
	int heartbeatSocket = socket(PF_INET, SOCK_DGRAM, 0);
    if(heartbeatSocket < 0)
    {
		close(heartbeatSocket);
        handleMessageAndExit("Failed to run socket for heartbeat.\n");
    }

    setSocketOptions(heartbeatSocket, 0);

	heartbeatAddress.sin_family = AF_INET;
    heartbeatAddress.sin_port = htons( (uint16_t)(atoi(heartbeatPort)) );
    heartbeatAddress.sin_addr.s_addr = htonl(INADDR_BROADCAST);

    int heartbeatBindOutput = bind(heartbeatSocket, (struct sockaddr *) &heartbeatAddress, sizeof(heartbeatAddress));
    if (heartbeatBindOutput < 0) 
    { 
		close(heartbeatSocket);
        handleMessageAndExit("Failed to bind heartbeat socket.\n");
    } 
    
	struct timeval recvTimeout;
    recvTimeout.tv_sec = 2;
    recvTimeout.tv_usec = 0;

	int opt2 = 1; 
    int setHeartbeatOptionsRecvOutput = setsockopt(heartbeatSocket, SOL_SOCKET, SO_RCVTIMEO, &recvTimeout, sizeof(recvTimeout));
    if (setHeartbeatOptionsRecvOutput < 0) 
    { 
		close(heartbeatSocket);
        handleMessageAndExit("Failed to set socket options for recieve in heartbeat.\n");
    }

	char buffer[BUFFER_SIZE];
	socklen_t heartbeatAddressSize = sizeof(heartbeatAddress);
	int recvFromOutput = recvfrom( heartbeatSocket, buffer, sizeof(buffer), 0, (struct sockaddr *) &heartbeatAddress, &heartbeatAddressSize);
	if(recvFromOutput < 0 ) {
		close(heartbeatSocket);
		return 0;
	}
	serverPort = atoi(buffer);
	close(heartbeatSocket);
	return 1;
}

char* readCommand() {
	char* command = (char*) malloc(MAX_COMMAND_SIZE * sizeof(char));
	read(0, command, MAX_COMMAND_SIZE);
	return command;
}

int findSpace(char* command) {
	int count = 0, founded = -1;
	for(int i = 0; i < strlen(command); i++) {
		if(command[i] == ' ') {
			count ++;
			founded = i;
		} 
	}
	if((count == -1) || (count != 1)) {
		return -1;
	}
	return founded;
}

int checkStringsEqualWithSpacePosition(char* a, char* b, int spacePosition) {
	if(spacePosition != strlen(b)) {
		return 0;
	}
	for(int i = 0; i < spacePosition; i++) {
		if(a[i] != b[i]) {
			return 0;
		}
	}
	return 1;
}

int checkStringsEqual(char* a, char* b) {
	if(strlen(a) != strlen(b)) {
		return 0;
	}
	for(int i = 0; i < strlen(a); i++) {
		if(a[i] != b[i]) {
			return 0;
		}
	}
	return 1;
}

void clearBuffer(char* buffer) {
    for(int i = 0; i < BUFFER_SIZE; i++) {
        buffer[i] = '\0';
    }
}

char* extractFileNameFromCommand(char* command, int spacePosition) {
	char* newCommand = command;
	newCommand[strlen(command) - 1] = '\0';
	char* fileName = (char*) malloc((strlen(newCommand) - spacePosition - 1) * sizeof(char));
	for (int i = spacePosition + 1; i < strlen(newCommand) ; i++) {
		fileName[i - spacePosition - 1] = newCommand[i];
	}
	return fileName;
}

void uploadFile(int clientSocket, char* fileName) {
	char* serverResponse = (char*) malloc(BUFFER_SIZE * sizeof(char));

	int file = open(fileName, O_RDONLY);
	if(file < 0) {
		handleMessage("The file with that name doesn't exist!\n");
		return;
	}
	
	char* commandMessage = "upload";
	int sendOutput = send(clientSocket, commandMessage, strlen(commandMessage), 0);
	if(sendOutput < 0) {
        handleMessageAndExit("Failed to send!\n");
    }

	int recvOutput = recv(clientSocket, serverResponse, BUFFER_SIZE, 0);
	if(recvOutput < 0) {
        handleMessageAndExit("Failed to recieve!\n");
    }
	clearBuffer(serverResponse);
	
	char* fileNameMessage = fileName;
	sendOutput = send(clientSocket, fileNameMessage, strlen(fileNameMessage), 0);
	if(sendOutput < 0) {
        handleMessageAndExit("Failed to send!\n");
    }

	recvOutput = recv(clientSocket, serverResponse, BUFFER_SIZE, 0);
	if(recvOutput < 0) {
        handleMessageAndExit("Failed to recieve!\n");
    }
	clearBuffer(serverResponse);

	char* buffer = (char*) malloc(BUFFER_SIZE * sizeof(char));

	while(read(file, buffer, BUFFER_SIZE) > 0) {
		sendOutput = send(clientSocket, buffer, strlen(buffer), 0);
		if(sendOutput < 0) {
			handleMessageAndExit("Failed to send!\n");
		}
		recvOutput = recv(clientSocket, serverResponse, BUFFER_SIZE, 0);
		if(recvOutput < 0) {
			handleMessageAndExit("Failed to recieve!\n");
		}
		clearBuffer(serverResponse);
		clearBuffer(buffer);
	}

	char* EOFMessage = "-->EOF<--";
	sendOutput = send(clientSocket, EOFMessage, strlen(EOFMessage), 0);
	if(sendOutput < 0) {
        handleMessageAndExit("Failed to send!\n");
    }
	recvOutput = recv(clientSocket, serverResponse, BUFFER_SIZE, 0);
	if(recvOutput < 0) {
        handleMessageAndExit("Failed to recieve!\n");
    }
	handleMessage(serverResponse);
	clearBuffer(serverResponse);
	close(file);
}

int downloadFile(int clientSocket, char* fileName) {
	char* okResponse = "1";
	char* serverResponse = (char*) malloc(BUFFER_SIZE * sizeof(char));
	char* buffer = (char*) malloc(BUFFER_SIZE * sizeof(char));
	
	char* commandMessage = "download";
	int sendOutput = send(clientSocket, commandMessage, strlen(commandMessage), 0);
	if(sendOutput < 0) {
        handleMessageAndExit("Failed to send!\n");
    }

	int recvOutput = recv(clientSocket, serverResponse, BUFFER_SIZE, 0);
	if(recvOutput < 0) {
        handleMessageAndExit("Failed to recieve!\n");
    }
	clearBuffer(serverResponse);
	
	char* fileNameMessage = fileName;
	sendOutput = send(clientSocket, fileNameMessage, strlen(fileNameMessage), 0);
	if(sendOutput < 0) {
        handleMessageAndExit("Failed to send!\n");
    }

	recvOutput = recv(clientSocket, serverResponse, BUFFER_SIZE, 0);
	if(recvOutput < 0) {
        handleMessageAndExit("Failed to recieve!\n");
    }
	if(atoi(serverResponse) == 0) {
		sendOutput = send(clientSocket, okResponse, strlen(okResponse), 0);
		if(sendOutput < 0) {
			handleMessageAndExit("Failed to send!\n");
		}
		handleMessage("The requested file doesn't exist on the server!\n");
		clearBuffer(serverResponse);
		return 0;
	}
	clearBuffer(serverResponse);

	unlink(fileName);
    int file = open(fileName, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);
    clearBuffer(fileName);

	while((recvOutput = recv(clientSocket, buffer, BUFFER_SIZE, 0)) > 0) {
        if(recvOutput < 0) {
            handleMessageAndExit("Failed to recieve!\n");
        }
        if(checkStringsEqual(buffer, "-->EOF<--")) {
            break;
        }
        write(file, buffer, strlen(buffer));
        sendOutput = send(clientSocket, okResponse, strlen(okResponse), 0);
        if(sendOutput < 0) {
            handleMessageAndExit("Failed to send!\n");
        }
        clearBuffer(buffer);
    }
    close(file);
    char* successMessage = "The file download was successful!\n";
    sendOutput = send(clientSocket, successMessage, strlen(successMessage), 0);
    if(sendOutput < 0) {
        handleMessageAndExit("Failed to send!\n");
    }
    handleMessage(successMessage);
	return 1;
}

void setSocketsAndBind() {

	//socket to connect to server
	if(serverStatus) {
		clientSocketForServer = socket(PF_INET, SOCK_STREAM, 0);
		if(clientSocketForServer < 0)
		{
			handleMessageAndExit("Failed to run socket.\n");
		}

		setSocketOptions(clientSocketForServer, 1);

		struct sockaddr_in clientAddreessForServer;
		clientAddreessForServer.sin_family = AF_INET;
		clientAddreessForServer.sin_port = htons(serverPort);
		clientAddreessForServer.sin_addr.s_addr = inet_addr(LOCAL_ADDRESS);
		socklen_t clientAddreessForServerSize = sizeof(clientAddreessForServer);

		int connectOutput = connect(clientSocketForServer, (struct sockaddr *) &clientAddreessForServer, clientAddreessForServerSize);
		if (connectOutput < 0) 
		{ 
			handleMessageAndExit("Failed to connect to server.\n");
		}

	}

	//socket to peer tcp connect

	clientSocketToTCPConnect = socket(PF_INET, SOCK_STREAM, 0);
    if(clientSocketToTCPConnect < 0)
    {
        handleMessageAndExit("Failed to run socket.\n");
    }

    setSocketOptions(clientSocketToTCPConnect, 1);

    struct sockaddr_in clientAddressToTCPConnect;
    clientAddressToTCPConnect.sin_family = AF_INET;
    clientAddressToTCPConnect.sin_port = htons((uint16_t)atoi(clientPort));
    clientAddressToTCPConnect.sin_addr.s_addr = inet_addr(LOCAL_ADDRESS);
    socklen_t clientAddressToTCPConnectSize = sizeof(clientAddressToTCPConnect);

    int tcpConnectBindOutput = bind(clientSocketToTCPConnect, (struct sockaddr *) &clientAddressToTCPConnect, clientAddressToTCPConnectSize);
    if (tcpConnectBindOutput < 0) 
    { 
        handleMessageAndExit("Failed to bind.\n");
    }
    
    int listenOutput = listen(clientSocketToTCPConnect, MAX_CLIENTS);
    if (listenOutput < 0)
    {
        handleMessageAndExit("Failed to listen.\n");
    }

	//socket to listen broadcasts

	broadcastSocket = socket(PF_INET, SOCK_DGRAM, 0);
	if(broadcastSocket < 0)
    {
        handleMessageAndExit("Failed to run socket.\n");
    }

	setSocketOptions(broadcastSocket, 0);

    broadcastAddress.sin_family = AF_INET;
    broadcastAddress.sin_port = htons((uint16_t)atoi(broadcastPort));
    broadcastAddress.sin_addr.s_addr = htonl(INADDR_BROADCAST);
    broadcastAddressSize = sizeof(broadcastAddress);

    int bindOutput = bind(broadcastSocket, (struct sockaddr*) &broadcastAddress, broadcastAddressSize);
	if(bindOutput < 0) {
		handleMessageAndExit("Error in binding.\n");
	}

}

int getMax3(int a, int b, int c) {
	int max = a;
	if( b > max ) max = b;
	if( c > max ) max = c;
	return max;
}

char* prepareRequestMessage(char* fileName) {
	char* reqMessage = (char*) malloc((5 + strlen(fileName)) * sizeof(char));
	for(int i = 0; i < (5 + strlen(fileName)); i++) {
        reqMessage[i] = '\0';
    }
	char* requestPrefix = "$req$";
	for(int i = 0; i < 5; i++) {
		reqMessage[i] = requestPrefix[i];
	}
	for(int i = 5; i < 5 + strlen(fileName); i++) {
		reqMessage[i] = fileName[i - 5];
	}
	return reqMessage;
}

char* prepareResponseMessage(char* fileName) {
	char* resMessage = (char*) malloc((5 + strlen(fileName) + strlen(clientPort) + 1) * sizeof(char));
	for(int i = 0; i < (5 + strlen(fileName) + strlen(clientPort) + 1); i++) {
        resMessage[i] = '\0';
    }
	char* requestPrefix = "$res$";
	for(int i = 0; i < 5; i++) {
		resMessage[i] = requestPrefix[i];
	}
	for(int i = 5; i < 5 + strlen(fileName); i++) {
		resMessage[i] = fileName[i - 5];
	}
	resMessage[5 + strlen(fileName)] = '#';
	for(int i = 0; i < strlen(clientPort); i++) {
		resMessage[6 + strlen(fileName) + i] = clientPort[i];
	}
	return resMessage;
}

void broadcastRequestedMessageFrequently() {
	socklen_t sendToOutput = sendto( broadcastSocket, broadcastRequestMessage, strlen(broadcastRequestMessage), 0, (struct sockaddr *) &broadcastAddress, broadcastAddressSize);
    if(sendToOutput != strlen(broadcastRequestMessage)) {
        handleMessageAndExit("Failed to send broadcast message.\n");
    }
	if(requestRecievedYet == 0) {
    	alarm(5);
	}
}

void requestToDownloadFileByPeerToPeer(char* fileName) {
	requestedFileNameForMe = fileName;
	broadcastRequestMessage = prepareRequestMessage(fileName);
	requestRecievedYet = 0;
	signal(SIGALRM, broadcastRequestedMessageFrequently);
	broadcastRequestedMessageFrequently();
}

void handleNewCommand() {
	char* command = readCommand();
	int prevStatus = serverStatus;
	serverStatus = checkForServer();
	if((prevStatus == 0) && (serverStatus == 1)){
		serverStatus = 0;
	}
	int spacePosition = findSpace(command);
	if(spacePosition == -1) {
		handleMessage("Wrong number of arguments for the command!\n");
		return;
	}
	char* fileName = extractFileNameFromCommand(command, spacePosition);
	if(checkStringsEqualWithSpacePosition(command, "upload", spacePosition)) {
		if(serverStatus == 0) {
			handleMessage("Server is not online. You can't upload.\n");
			return;
		}
		uploadFile(clientSocketForServer, fileName);
	}
	else if(checkStringsEqualWithSpacePosition(command, "download", spacePosition)) {
		if(serverStatus == 0) {
			handleMessage("Downloading file by peer to peer mode...\n");
			requestToDownloadFileByPeerToPeer(fileName);
			return;
		}
		int isFileAvailable = downloadFile(clientSocketForServer, fileName);
		if(isFileAvailable == 0) {
			handleMessage("Downloading file by peer to peer mode...\n");
			requestToDownloadFileByPeerToPeer(fileName);
		}
	}
	else {
		handleMessage("Command not found!\n");
	}
}


void handleTCPAccept(fd_set *tempReadSockets, int *maxSocketNumber) {
	struct sockaddr_in * newClientAcceptAddress = ( struct sockaddr_in* ) malloc( sizeof(struct sockaddr_in) );
    socklen_t newClientAcceptAddressSize = sizeof(*newClientAcceptAddress);
	int newClientSocket = accept(clientSocketToTCPConnect, ( struct sockaddr* ) newClientAcceptAddress, ( socklen_t* )&newClientAcceptAddressSize);
	if (newClientSocket < 0)
	{
		handleMessageAndExit("Failed to accept new client.\n");
	}
	FD_SET(newClientSocket, tempReadSockets);
	if (newClientSocket > *maxSocketNumber) {
		*maxSocketNumber = newClientSocket;
	}
	
}

int checkIfReq(char* message) {
	char* requestPattern = "$req$";
	for(int i = 0; i < 5; i++) {
		if(message[i] != requestPattern[i]) {
			return 0;
		}
	}
	return 1;
}

int checkIfRes(char* message) {
	char* answerPattern = "$res$";
	for(int i = 0; i < 5; i++) {
		if(message[i] != answerPattern[i]) {
			return 0;
		}
	}
	return 1;
}

int getHashtagPosition(char* buffer) {
	for(int i = 0; i < strlen(buffer); i++) {
		if(buffer[i] == '#') {
			return i;
		}
	}
	return -1;
}

char* getFileNameFromResponse(char* buffer, int hashtagPosition) {
	char* fileName = (char*) malloc((hashtagPosition + 1) * sizeof(char));
	for(int i = 0; i < hashtagPosition; i++) {
		fileName[i] = buffer[i];
	}
	fileName[hashtagPosition] = '\0';
	return fileName;
}

int getPortNumber(char* buffer, int hashtagPosition) {
	char* portNumber = (char*) malloc((strlen(buffer) - hashtagPosition) * sizeof(char));
	for(int i = hashtagPosition + 1; i < strlen(buffer); i++) {
		portNumber[i - hashtagPosition - 1] = buffer[i];
	}
	portNumber[hashtagPosition] = '\0';
	return atoi(portNumber);
}

void handleListenBroadcast() {
	char* buffer = (char*) malloc(BUFFER_SIZE * sizeof(char));

	clearBuffer(buffer);
	recvfrom(broadcastSocket, buffer, BUFFER_SIZE, 0, (struct sockaddr*)& broadcastAddress, &broadcastAddressSize);
	broadcastAddress.sin_addr.s_addr = INADDR_BROADCAST;

	if(checkIfReq(buffer)) {
		char* fileName = &(buffer[5]);
		if(checkStringsEqual(fileName, requestedFileNameForMe) == 1) {
			return;
		}
		int file = open(fileName, O_RDONLY, S_IRUSR | S_IWUSR);
		if(file < 0) {
			clearBuffer(buffer);
			return;
		}
		char* responseMessage = prepareResponseMessage(fileName);
		int sendtoOutput = sendto(broadcastSocket, responseMessage, strlen(responseMessage), 0,(struct sockaddr*)& broadcastAddress, broadcastAddressSize);
		if(sendtoOutput != strlen(responseMessage)) {
			handleMessageAndExit("Failed to send heartbeat message.\n");
		}
		clearBuffer(buffer);
	}
	else if(checkIfRes(buffer)) {
		char* recievedData = (char*) malloc(BUFFER_SIZE * sizeof(char));
		clearBuffer(recievedData);
		char* pointerForFreeing = buffer;
		buffer = &(buffer[5]);
		int hashtagPosition = getHashtagPosition(buffer);
		if(hashtagPosition < 0) {
			handleMessageAndExit("InvalidResponseSyntax\n");
		}
		char* fileName = getFileNameFromResponse(buffer, hashtagPosition);
		if(checkStringsEqual(fileName, requestedFileNameForMe) == 0) {
			clearBuffer(pointerForFreeing);
			return;
		}
		int otherClientPortNumber = getPortNumber(buffer, hashtagPosition);

		int socketToConnectToOtherClient = socket(PF_INET, SOCK_STREAM, 0);
		if(socketToConnectToOtherClient < 0)
		{
			handleMessageAndExit("Failed to run socket.\n");
		}

		setSocketOptions(socketToConnectToOtherClient, 1);

		struct sockaddr_in clientAddressToConnectToOtherClient;
		clientAddressToConnectToOtherClient.sin_family = AF_INET;
		clientAddressToConnectToOtherClient.sin_port = htons( otherClientPortNumber );
		clientAddressToConnectToOtherClient.sin_addr.s_addr = inet_addr(LOCAL_ADDRESS);
		socklen_t clientAddressToConnectToOtherClientSize = sizeof(clientAddressToConnectToOtherClient);

		int connectOutput = connect(socketToConnectToOtherClient, (struct sockaddr *) &clientAddressToConnectToOtherClient, clientAddressToConnectToOtherClientSize);
		if (connectOutput < 0) 
		{ 
			handleMessageAndExit("Failed to connect to server.\n");
		}

		char* otherClientResponse = (char*) malloc(BUFFER_SIZE * sizeof(char));
		clearBuffer(otherClientResponse);
		char* okResponse = "1";
		int sendOutput = send(socketToConnectToOtherClient, fileName, strlen(fileName), 0);
		if(sendOutput < 0) {
			handleMessageAndExit("Failed to send!\n");
		}
		int recvOutput = recv(socketToConnectToOtherClient, otherClientResponse, BUFFER_SIZE, 0);
		if(recvOutput < 0) {
			handleMessageAndExit("Failed to recieve!\n");
		}
		clearBuffer(otherClientResponse);
		unlink(fileName);
		int file = open(fileName, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);

		while((recvOutput = recv(socketToConnectToOtherClient, recievedData, BUFFER_SIZE, 0)) > 0) {
			if(recvOutput < 0) {
				handleMessageAndExit("Failed to recieve!\n");
			}
			if(checkStringsEqual(recievedData, "-->EOF<--")) {
				break;
			}
			write(file, recievedData, strlen(recievedData));
			sendOutput = send(socketToConnectToOtherClient, okResponse, strlen(okResponse), 0);
			if(sendOutput < 0) {
				handleMessageAndExit("Failed to send!\n");
			}
			clearBuffer(recievedData);
		}
		close(file);
		char* successMessage = "The file download was successful!\n";
		sendOutput = send(socketToConnectToOtherClient, successMessage, strlen(successMessage), 0);
		if(sendOutput < 0) {
			handleMessageAndExit("Failed to send!\n");
		}
		handleMessage(successMessage);
		close(socketToConnectToOtherClient);
		clearBuffer(pointerForFreeing);
		requestedFileNameForMe = "";
		requestRecievedYet = 1;
		
	}


}

void handleSendingFileWhenFound(fd_set *tempReadSockets, int socketNumberOfRequestingClient) {
	char* okResponse = "1";
	char fileName[BUFFER_SIZE];
	char* clientResponse = (char*) malloc(BUFFER_SIZE * sizeof(char));
    char* buffer = (char*) malloc(BUFFER_SIZE * sizeof(char));
	int recvOutput = recv(socketNumberOfRequestingClient, fileName, BUFFER_SIZE, 0);
	if(recvOutput < 0) 
	{
		close(socketNumberOfRequestingClient);
		handleMessageAndExit("Failed to recieve from client.\n");
	}
	else if (recvOutput == 0)
	{
		close(socketNumberOfRequestingClient);
		FD_CLR(socketNumberOfRequestingClient, tempReadSockets);
	}
	else {
		int file = open(fileName, O_RDONLY, S_IRUSR | S_IWUSR);
		int sendOutput = send(socketNumberOfRequestingClient, okResponse, strlen(okResponse), 0);
		if(sendOutput < 0) {
			handleMessageAndExit("Failed to send!\n");
		}
		while(read(file, buffer, BUFFER_SIZE) > 0) {
			sendOutput = send(socketNumberOfRequestingClient, buffer, strlen(buffer), 0);
			if(sendOutput < 0) {
				handleMessageAndExit("Failed to send!\n");
			}
			recvOutput = recv(socketNumberOfRequestingClient, clientResponse, BUFFER_SIZE, 0);
			if(recvOutput < 0) {
				handleMessageAndExit("Failed to recieve!\n");
			}
			clearBuffer(clientResponse);
			clearBuffer(buffer);
		}
		close(file);

		char* EOFMessage = "-->EOF<--";
		sendOutput = send(socketNumberOfRequestingClient, EOFMessage, strlen(EOFMessage), 0);
		if(sendOutput < 0) {
			handleMessageAndExit("Failed to send!\n");
		}
		recvOutput = recv(socketNumberOfRequestingClient, clientResponse, BUFFER_SIZE, 0);
		if(recvOutput < 0) {
			handleMessageAndExit("Failed to recieve!\n");
		}
		
		handleMessage(clientResponse);
		clearBuffer(clientResponse);
		
	}
}

void startReadingCommands() {
	fd_set mainReadSockets, tempReadSockets;
    FD_ZERO(&mainReadSockets);
    FD_ZERO(&tempReadSockets);
    FD_SET(broadcastSocket, &tempReadSockets);
	FD_SET(clientSocketToTCPConnect, &tempReadSockets);
	FD_SET(STDIN_FILENO, &tempReadSockets);
    int maxSocketNumber = getMax3(broadcastSocket, clientSocketToTCPConnect, STDIN_FILENO);
	while(1) {
		mainReadSockets = tempReadSockets;
        int selectOutput = select(maxSocketNumber + 1, &mainReadSockets, NULL, NULL, NULL);
        if(selectOutput < 0) {
            continue;
        }
		for(int i = 0; i < maxSocketNumber + 1; i++) {
            if (FD_ISSET(i, &mainReadSockets)) {
				if(i == STDIN_FILENO) {
					handleNewCommand();
				}
				else if(i == clientSocketToTCPConnect) {
					handleTCPAccept(&tempReadSockets, &maxSocketNumber);
				}
				else if(i == broadcastSocket) {
					handleListenBroadcast();
				}
				else {
					handleSendingFileWhenFound(&tempReadSockets, i);
				}
			}
		}
	}
}

void startConnection() {
	setSocketsAndBind();
	startReadingCommands();
}

int main(int argc, char const *argv[]) 
{ 
	if(argc != 4) {
        handleMessageAndExit("The number of arguments passed is not correct!!\n");
    }

	heartbeatPort = argv[1];
	broadcastPort = argv[2];
	clientPort = argv[3];

	requestedFileNameForMe = (char*) malloc(BUFFER_SIZE * sizeof(char));
	clearBuffer(requestedFileNameForMe);

	handleMessage("Searching for the server...\n");
	serverStatus = checkForServer();
	if(serverStatus) {
		handleMessage("Server found.\n");
	}
	else {
		handleMessage("Server not found.\n");
	}

	startConnection();

	exit(EXIT_SUCCESS);
} 
